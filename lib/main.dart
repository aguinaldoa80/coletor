import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  WebViewController _webViewController;
  String filePath = 'files/index.html';
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: WebView(
        //initialUrl: 'http://www.google.com',
        //initialUrl: 'files/teste.html',
        initialUrl: 'www.google.com',
        //initialUrl: '',
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController){
          _webViewController = webViewController;
          //_loadHtmlFromAssets();
        },
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Increment',
        child: Icon(Icons.access_alarm),
        onPressed: (){
          print("Butao pressionado");
          //_webViewController.loadUrl('file:files/index.html');
          String command = 'Aguinaldo \n..Aguinaldo...';
          //_webViewController.evaluateJavascript('term.clear()');
          //_webViewController.evaluateJavascript('term.write($command)');
          //_webViewController.evaluateJavascript('term.clear()');
          //_webViewController.evaluateJavascript('term.write("Hello from \x1B[1;3;31mxterm.js\x1B[0m")');
          //_webViewController.evaluateJavascript("term.write('\x1b[m\x08\x1b[1m     4\x1b[m\x1b[H\x1b[K\x1b[3;7H20/04/19\x1b[H')");
          //_webViewController.evaluateJavascript("term.write('\x1B\x5B\x31\x6D\x32\x1B\x5B\x6D\x1B\x5B\x35\x3B\x31\x48\x1B\x5B\x4B\x1B\x5B\x48\x1B\x5B\x43\x32\x1B\x5B\x48\x1B\x5B\x4B\x0A\x1B\x5B\x4B\x0A\x1B\x5B\x4B\x0A\x1B\x5B\x4B\x1B\x5B\x36\x3B\x31\x48\x1B\x5B\x4B\x0A\x1B\x5B\x4B\x1B\x5B\x48\x43\x6F\x6E\x66\x65\x72\x65\x6E\x63\x69\x61\x20\x20\x45\x6E\x74\x72\x61\x64\x61\x1B\x5B\x48\x0A\x5B\x20\x5D\x1B\x5B\x33\x3B\x32\x48\x31\x2D\x20\x43\x6F\x6E\x66\x65\x72\x65\x6E\x63\x69\x61\x1B\x5B\x34\x3B\x32\x48\x32\x2D\x20\x43\x6F\x6E\x73\x75\x6C\x74\x61\x73\x1B\x5B\x35\x3B\x32\x48\x33\x2D\x20\x52\x65\x6C\x61\x74\x6F\x72\x69\x6F\x1B\x5B\x36\x3B\x32\x48\x34\x2D\x20\x52\x65\x69\x6E\x69\x63\x69\x61\x20\x43\x6F\x6E\x66\x2E\x1B\x5B\x37\x3B\x32\x48\x35\x2D\x20\x4C\x69\x62\x65\x72\x61\x20\x43\x6F\x6E\x66\x2E\x1B\x5B\x38\x3B\x32\x48\x39\x2D\x20\x46\x69\x6E\x61\x6C\x69\x7A\x61\x1B\x5B\x48\x0A\x1B\x5B\x43\x1B\x5B\x31\x6D\x30\x1B\x5B\x6D\x08')");
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  _loadHtmlFromAssets() async {
    String fileHtmlContents = await rootBundle.loadString(filePath);
    //String fileHtmlContents = '../files/teste.html';
    _webViewController.loadUrl(Uri.dataFromString(fileHtmlContents,mimeType: 'text/html', 
    encoding: Encoding.getByName('utf-8')).toString());
  }
}
